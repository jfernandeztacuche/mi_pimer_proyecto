module capacitacion

go 1.17

require (
	github.com/buaazp/fasthttprouter v0.1.1
	github.com/valyala/fasthttp v1.38.0
)

require (
	github.com/andybalholm/brotli v1.0.4 // indirect
	github.com/klauspost/compress v1.15.9 // indirect
	github.com/lib/pq v1.10.6 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
)
