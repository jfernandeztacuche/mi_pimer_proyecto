package main

import (
	"capacitacion/application/routers"
	"fmt"
	"log"

	"github.com/valyala/fasthttp"
)

type Cliente struct {
	Nombre    string
	Apellido  string
	Direccion string
	Poliza    string
}

func main() {

	fmt.Println("Ejecutando ...")
	router := routers.NewHttpRouter()
	log.Fatal(fasthttp.ListenAndServe(":8090", router.Handler()))

}

func GetPolicy() string {
	return "Hola José"
}
