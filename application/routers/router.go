package routers

import (
	"capacitacion/application/container"
	"capacitacion/application/controllers"

	"github.com/buaazp/fasthttprouter"
	"github.com/valyala/fasthttp"
)

//creamos la clase
type httpRouter struct {
	controladorPrincipal *controllers.ControladorPrincipal
}

//metodo constructor
func NewHttpRouter() *httpRouter {

	return &httpRouter{
		controladorPrincipal: container.ControladorListarDatos(),
	}

}

func (httpRouter *httpRouter) Handler() fasthttp.RequestHandler {

	router := fasthttprouter.New()
	router.GET("/getList", httpRouter.controladorPrincipal.GetList)

	return router.Handler

}
