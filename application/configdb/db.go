package configdb

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
)

type PostgresConfig struct {
}

func NewPostgresConfig() *PostgresConfig {
	return &PostgresConfig{}
}

func (pgsql *PostgresConfig) GetSQLConnection() (*sql.DB, error) {
	fmt.Println("Conexion a la bd PGSQL")

	dsn := fmt.Sprintf("postgres://%s:%s@%s:%v/%s?sslmode=disable",
		"usrplaft",
		"postgres",
		"S92901VDP",
		"5432",
		"BDPLAFT")

	db, err := sql.Open("postgres", dsn)

	return db, err
}
