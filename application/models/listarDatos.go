package models

type ResponseListarDatos struct {
	IdCalAlerta int64  `json:"id_cal_alerta"`
	Nombre      string `json:"nombre"`
	Activo      int64  `json:"activo"`
}
