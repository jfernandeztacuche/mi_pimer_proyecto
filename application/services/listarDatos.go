package services

import (
	"capacitacion/application/storage"
	"fmt"
)

type ListarDatosServices struct {
	listStorage storage.IListarDataStorage
}

func NewListarDatosServices(listStorage storage.IListarDataStorage) *ListarDatosServices {

	return &ListarDatosServices{
		listStorage: listStorage,
	}

}

func (l *ListarDatosServices) GetListDataS() bool {

	fmt.Println("inicio de servicio")

	respuesta := l.listStorage.GetListDataSt()

	return respuesta

}
