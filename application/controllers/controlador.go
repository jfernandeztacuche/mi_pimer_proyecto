package controllers

import (
	"capacitacion/application/services"
	"fmt"

	"github.com/valyala/fasthttp"
)

//creamos la clase

type ControladorPrincipal struct {
	listService services.IListarData
}

func NewControladorPrincipal(listService services.IListarData) *ControladorPrincipal {
	return &ControladorPrincipal{
		listService: listService,
	}
}

func (c *ControladorPrincipal) GetList(ctx *fasthttp.RequestCtx) {

	fmt.Println("Inicio controlador")

	fmt.Println("Prepara")

	respuesta := c.listService.GetListDataS()

	fmt.Println(respuesta)

}
