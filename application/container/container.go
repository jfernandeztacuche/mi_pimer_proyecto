package container

import (
	"capacitacion/application/configdb"
	"capacitacion/application/controllers"
	"capacitacion/application/services"
	"capacitacion/application/storage"
)

func ControladorListarDatos() *controllers.ControladorPrincipal {
	return controllers.NewControladorPrincipal(GetListDataService())
}

func GetListDataService() services.IListarData {
	return services.NewListarDatosServices(GetListDataStorage())
}

func GetListDataStorage() storage.IListarDataStorage {
	return storage.NewListarDatosStorage(PostgresConexion())
}

func PostgresConexion() *configdb.PostgresConfig {
	return configdb.NewPostgresConfig()
}
