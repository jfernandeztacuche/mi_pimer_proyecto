package storage

import (
	"capacitacion/application/configdb"
	"capacitacion/application/models"
	"fmt"
)

type ListarDatosStorage struct {
	conexion *configdb.PostgresConfig
}

// GetListarDataSt implements IListarDataStorage
func (*ListarDatosStorage) GetListarDataSt() bool {
	panic("unimplemented")
}

func NewListarDatosStorage(conexion *configdb.PostgresConfig) *ListarDatosStorage {
	return &ListarDatosStorage{
		conexion: conexion,
	}
}

func (l *ListarDatosStorage) GetListDataSt() bool {

	fmt.Println("Inicio storage")

	var resp models.ResponseListarDatos
	var registros []models.ResponseListarDatos

	//conexion a la bd
	db, err := l.conexion.GetSQLConnection()
	defer db.Close()

	if err != nil {
		fmt.Println(err)
		panic(err)
	}

	//con solo query necesitas iterar
	rows, err := db.Query("select id_cal_alerta,nombre,activo from cal_alerta limit 5")
	defer rows.Close()

	if err != nil {
		fmt.Println(err)
		panic(err)
	}

	for rows.Next() {
		rows.Scan(&resp.IdCalAlerta, &resp.Nombre, &resp.Activo)
		registros = append(registros, resp)

	}

	fmt.Println("------ registros --------")
	fmt.Println(registros)
	fmt.Println("------ registros --------")

	return false

}
